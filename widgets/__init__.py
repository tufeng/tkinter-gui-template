from .button import *
from .label import *
from .frame import *
from .entry import *
from .splashscreen import *