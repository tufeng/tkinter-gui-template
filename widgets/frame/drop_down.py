import tkinter as tk
import tkinter.ttk as ttk
from ttkbootstrap import Style

class DropDown(ttk.Frame):
    def __init__(self, *args, **kwargs):
        ttk.Frame.__init__(self, style="primary.TFrame", *args, **kwargs)
        self.pack(fill=tk.BOTH)

        self.is_show = False

        self.dropdown_button_bar = ttk.Frame(self, height=50, style="primary.TFrame")
        self.dropdown_button_bar.pack(fill=tk.X, expand=False)

        self.label = ttk.Label(self.dropdown_button_bar, text='button')
        self.label.pack()

        self.dropdown = ttk.Frame(self, height=100, style="danger.TFrame")

        self.bind_tree(self.dropdown_button_bar, '<Button-1>', self.show)
        #self.dropdown_button_bar.bind('<Button-1>', self.show)

    def test(self, event):
        print('click')

    def show(self, event):
        if self.is_show:
            self.is_show = False
            self.dropdown.pack_forget()
        else:
            self.is_show = True
            self.dropdown.pack(fill="both", expand=False)

    def get_content(self):
        return self.dropdown

    def get_button(self):
        return self.dropdown_button

    def bind_tree(self, widget, event, callback, add=''):
        "Binds an event to a widget and all its descendants."

        widget.bind(event, callback, add)

        for child in widget.children.values():
            self.bind_tree(child, event, callback)
