import tkinter as tk
import os

from PIL import ImageTk, Image
from tkinter import ttk

class Menu(ttk.Frame):
    def __init__(self, *args, **kwargs):
        ttk.Frame.__init__(self, *args, **kwargs)

        self.name = None
        self.show = False
        self.current_page = None

        self.buttons = {}
        self.imgs = {}

        self.assets_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '../modules/assets/icons'))

    def get_image(self, img_path):
        img = Image.open(img_path)
        img = img.resize((25, 25), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)

        return img

    def add_button(self, frame=None, img_path=None, width=None, text="Menu button", command=None, style="TButton",
                   side=tk.LEFT, fill=None, expand=False, compound="none", parents=None):

        if img_path is not None:
            self.imgs[text] = self.get_image(self.assets_folder + '/' + img_path)
            self.buttons[text] = ttk.Button(frame, image=self.imgs[text], text=text, compound=compound, style=style, command=command)

            self.buttons[text].bind('<Button-1>', lambda event, parents=parents, button=self.buttons[text], text=text: self.button_status(parents=parents, button=button, text=text))

            self.buttons[text].pack(side=side, fill=fill, expand=expand)
        else:
            self.buttons[text] = ttk.Button(frame, text=text, compound=compound, style=style, command=command)
            self.buttons[text].pack(side=side, fill=fill, expand=expand)

        if style != 'TButton':
            parents.current_page_button = self.buttons[text]

    def button_status(self, parents=None, button=None, text=None):
        button.configure(style="info.TButton")

        if parents.current_page_button is None:
            button.configure(style="info.TButton")
            parents.current_page_button = button

        if parents.current_page_button is not None:
            parents.current_page_button.configure(style="TButton")
            button.configure(style="info.TButton")
            parents.current_page_button = button

        #
        # parent.current_page_button = buttons[text]
        # print(parent.current_page_button)
        # for button in buttons:
        #     if self.current_page == button:
        #         buttons[button].configure(style="info.TButton")
        #     else:
        #         buttons[button].configure(style="TButton")

        #button.configure(style="info.TButton")