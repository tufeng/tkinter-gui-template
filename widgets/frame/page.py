import tkinter as tk
import tkinter.ttk as ttk

class Page(ttk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)

        self.menu = None
        self.is_show = False
        self.in_menu = None
        self.icon = "default.png"

    def show(self):
        self.lift()

    def set_is_show(self, value):
        self.is_show = value

    def set_in_menu(self, value):
        self.in_menu = value

    def set_icon(self, value):
        self.icon = value

    def get_is_show(self):
        return self.is_show

    def get_in_menu(self):
        return self.in_menu

    def get_icon(self):
        return self.icon