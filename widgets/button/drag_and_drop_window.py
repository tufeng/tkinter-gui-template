import tkinter as tk
import os

from PIL import ImageTk, Image
from tkinter import ttk

class DragAndDropWindow(ttk.Button):
    def __init__(self, window, *args, **kwargs):
        ttk.Button.__init__(self, text="drag", *args, **kwargs)
        self.lastClickX = 0
        self.lastClickY = 0

        self.bind('<Button-1>', self.save_last_clickpos)
        self.bind('<B1-Motion>', self.dragging)

        self.pack()
        self.window = window


    def save_last_clickpos(self, event):
        self.lastClickX = event.x
        self.lastClickY = event.y

    def dragging(self, event):
        x, y = event.x - self.lastClickX + self.window.winfo_x(), event.y - self.lastClickY + self.window.winfo_y()
        self.window.geometry("+%s+%s" % (x, y))