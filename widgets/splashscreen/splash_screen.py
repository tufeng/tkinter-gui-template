import tkinter as tk
import tkinter.ttk as ttk
import time

from ttkbootstrap import Style

class SplashScreen(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Gets the requested values of the height and widht.
        windowWidth = 400
        windowHeight = 200

        screenWidth = self.winfo_screenwidth()
        screenHeight = self.winfo_screenheight()

        x = (screenWidth / 2) - (windowWidth / 2)
        y = (screenHeight / 2) - (windowHeight / 2)

        self.title("Splash Screen")
        self.overrideredirect(True)
        self.geometry(f"{windowWidth}x{windowHeight}+{int(x)}+{int(y)}")

        self.style = Style()

        self.label = ttk.Label(self, text="splash screen", font="Helvetica, 40")
        self.label.pack(pady=(40,0))

        self.progress_bar = ttk.Progressbar(self, value=0, length=300, style='Striped.Horizontal.TProgressbar')
        self.progress_bar.pack(side=tk.BOTTOM, pady=(0,30))
        self.progress_bar.start(4)
        self.after(500, self.stop_splash)

        self.mainloop()

    def stop_splash(self):
        self.progress_bar.stop()
        self.destroy()