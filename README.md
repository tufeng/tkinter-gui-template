# Tkinter Gui Template

## Description
___
This is a simple tkinter gui template for python.
This can be used to easly creating an software with gui using tkinter.
Any recommandation to improve the code is welcomed.

## Features
___
Incoming...

## Versions
___
`Python 3.9`

## Commands
```
#Generate virtual environment
    $ python3 -m venv env

#Activate virtual environment
    $ source env/bin/activate

#Install all necessary packages
    $ pip install -r requirements.txt
#Launch the app
    $ python3 main.py
```

## Design
___
Figma link incoming...

## License
This project is licensed under [Apache 2.0]("./LICENSE.md")