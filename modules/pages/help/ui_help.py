from widgets import Page
import tkinter as tk

class Help(Page):
    def __init__(self, parent, *args, **kwargs):
        Page.__init__(self, *args, **kwargs)
        self.menu = "top_menu"
        self.icon = "help.png"
        label = tk.Label(self, text="Help page")
        label.pack(fill=tk.BOTH, expand=True)
        self.configure(bg="green")