from widgets import Page
import tkinter as tk

class SamplePage(Page):
    def __init__(self, parent, *args, **kwargs):
        Page.__init__(self, *args, **kwargs)
        self.menu = None
        self.is_show = False
        label = tk.Label(self, text="This is an sample page")
        label.pack(fill = tk.BOTH, expand = True)
        self.configure(bg="orange")