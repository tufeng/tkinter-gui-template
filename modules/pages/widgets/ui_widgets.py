from widgets import Page
from widgets import DragAndDropWindow
from widgets import DropDown

import tkinter as tk
import tkinter as ttk

class Widgets(Page):
    def __init__(self, parent, *args, **kwargs):
        Page.__init__(self, *args, **kwargs)
        self.menu = "left_menu"
        self.is_show = True
        self.icon = "cil-options-horizontal.png"
        label = tk.Label(self, text="This page show display differents widgets")
        label.pack(fill = tk.X, expand = False)

        self.configure(bg="orange")

        self.drag_btn = DragAndDropWindow(master=self, window=parent.parent)

        self.drop = DropDown(self)
        self.drop2 = DropDown(self)

        self.label_drop_1 = ttk.Label(self.drop.get_content(), text="drop 1")
        self.label_drop_1.pack()
        self.label_drop_2 = ttk.Label(self.drop2.get_content(), text="drop 2")
        self.label_drop_2.pack()

    def open(self):
        test = tk.Toplevel(cursor="man", menu=True)
        test.geometry("50x50")
        test.mainloop()