from widgets import Page
import tkinter as tk

class Main(Page):
    def __init__(self, parent, *args, **kwargs):
        Page.__init__(self, *args, **kwargs)
        self.menu = "left_menu"
        self.icon = "cil-house.png"
        self.is_show = True
        label = tk.Label(self, text="Main page")
        label.pack(fill=tk.BOTH, expand=True)
        self.configure(bg="orange")