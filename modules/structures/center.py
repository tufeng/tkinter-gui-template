import tkinter as tk
import os

from ..pages import *

class Center(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.parent = parent

        self.configure(height="50")
        self.configure(bg="light blue")

        self.frames_pages = {}

        self.page_path = "pages"

        # PATH TOP
        if parent.top or parent.left:
            self.create_page()

        for object in self.frames_pages:
            if self.frames_pages[object]['page'].is_show:
                self.frames_pages[object]['page'].show()

    def create_page(self):
        self.path_top = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', self.page_path))

        dirs = [d for d in os.listdir(self.path_top)]
        for d in dirs:
            if d != "__init__.py" and d != "__pycache__":
                object = self.to_upper_camel_case(d)

                page = eval(object)(self)

                self.frames_pages[object] = {"page": page,
                                             "icon": page.icon,
                                             "name": self.to_capitalize_string(d)}
                self.frames_pages[object]["page"].place(in_=self, x=0, y=0, relwidth=1, relheight=1)

    def get_pages(self, menu=None):
        frames_pages = {}
        if menu is not None:
            for frame_page in self.frames_pages:
                if self.frames_pages[frame_page]["page"].menu == menu:
                    frames_pages[frame_page] = self.frames_pages[frame_page]

        return frames_pages

    def to_upper_camel_case(self, val):
        value = val.split('_')
        return value[0].capitalize() + ''.join(ele.title() for ele in value[1:])

    def to_capitalize_string(self, val):
        value = val.split('_')
        return value[0].capitalize() + ' ' + ''.join(ele for ele in value[1:])
