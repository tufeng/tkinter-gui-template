import tkinter as tk
import os

from widgets import Menu

class SampleMenu(Menu):
    def __init__(self, parent, *args, **kwargs):
        Menu.__init__(self, bg="blue", *args, **kwargs)
        self.show = False
        self.name = "sample_menu"

        self.img = self.get_image(self.assets_folder + '/default.png')

        for page in self.top_page:
            self.add_button(self,
                            side=tk.TOP,
                            img_path=self.top_page[page]['page'].icon,
                            text=self.top_page[page]['name'],
                            bg='red',
                            fill=tk.BOTH,
                            command=self.top_page[page]['page'].lift)