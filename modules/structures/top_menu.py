import tkinter as tk
import tkinter.ttk as ttk
import os

from widgets import Menu
from tkinter import ttk

class TopMenu(Menu):
    def __init__(self, parent, *args, **kwargs):
        Menu.__init__(self, style="primary.TFrame", *args, **kwargs)
        self.show = True
        self.name = "top_menu"

        self.top_page = parent.center.get_pages(self.name)

        #self.add_button(self,
        #                side=tk.TOP,
        #                text="test",
        #                fill=tk.BOTH,
        #                command=self.test2(parent))

        for page in self.top_page:
            if self.top_page[page]['page'].is_show:
                self.add_button(self,
                                side=tk.RIGHT,
                                style='info.TButton',
                                img_path=self.top_page[page]['page'].icon,
                                text=self.top_page[page]['name'],
                                command=self.top_page[page]['page'].lift,
                                parents=parent)
            else:
                self.add_button(self,
                                side=tk.RIGHT,
                                img_path=self.top_page[page]['page'].icon,
                                text=self.top_page[page]['name'],
                                command=self.top_page[page]['page'].lift,
                                parents=parent)

        self.logo_img = self.get_image(self.assets_folder + '/default.png')

        self.app_name_frame = ttk.Frame(self, style="primary.TFrame")
        self.app_name_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=False)

        self.logo = ttk.Label(self.app_name_frame, image=self.logo_img, text="menu", style="primary.Inverse.TLabel")
        self.logo.pack(side=tk.LEFT, fill=tk.BOTH, expand=False)

        self.title = ttk.Label(self.app_name_frame, text="menu", style="primary.Inverse.TLabel")
        self.title.pack(side=tk.RIGHT, fill=tk.BOTH, expand=False)

    def test2(self, parent):
        print('top')
        print(parent.orange)
        parent.orange = "rouge"
        print(parent.orange)