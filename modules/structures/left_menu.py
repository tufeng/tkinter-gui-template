import tkinter as tk
import os

from widgets import Menu
from tkinter import ttk

class LeftMenu(Menu):
    def __init__(self, parent, *args, **kwargs):
        Menu.__init__(self, parent, style="primary.TFrame", *args, **kwargs)
        self.show = True
        self.name = "left_menu"
        self.left_toggle_menu_is_on = False

        self.img = self.get_image(self.assets_folder + '/default.png')

        self.toggle_button = ttk.Button(self, image=self.img, text="Menu", command=self.toggle)
        self.toggle_button.pack(side=tk.TOP, fill=tk.X, expand=False)

        self.left_page = parent.center.get_pages(self.name)

        for page in self.left_page:
            if self.left_page[page]['page'].is_show:
                self.add_button(self,
                                side=tk.TOP,
                                img_path=self.left_page[page]['page'].icon,
                                text=self.left_page[page]['name'],
                                fill=tk.BOTH,
                                style='info.TButton',
                                command=self.left_page[page]['page'].lift,
                                parents=parent)
            else:
                self.add_button(self,
                                side=tk.TOP,
                                img_path=self.left_page[page]['page'].icon,
                                text=self.left_page[page]['name'],
                                fill=tk.BOTH,
                                command=self.left_page[page]['page'].lift,
                                parents=parent)

    def toggle(self):
        if self.left_toggle_menu_is_on:
            self.toggle_button.configure(compound="none")
            for b in self.buttons:
                self.buttons[b].configure(compound="none")
            self.left_toggle_menu_is_on = False
        else:
            self.toggle_button.configure(compound="left")
            for b in self.buttons:
                self.buttons[b].configure(compound="left")
            self.left_toggle_menu_is_on = True
