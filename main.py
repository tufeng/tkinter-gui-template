import tkinter as tk

from modules import *
from widgets import SplashScreen
from tkinter import ttk
from ttkbootstrap import Style

class MainWindow(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Gets the requested values of the height and widht.
        windowWidth = 400
        windowHeight = 600

        screenWidth = self.winfo_screenwidth()
        screenHeight = self.winfo_screenheight()

        x = (screenWidth / 2) - (windowWidth / 2)
        y = (screenHeight / 2) - (windowHeight / 2)

        self.title("Splash Screen")
        self.geometry(f"{windowWidth}x{windowHeight}+{int(x)}+{int(y)}")

        self.minsize(400, 400)
        self.maxsize(600, 600)

        self.style = Style()
        self.style.configure('TButton', anchor=tk.W)

        self.configure(bg="#fffbe2")

        self.top = True
        self.left = True
        self.current_page_button = None

        self.center = Center(self)
        self.orange = "pomme"

        if self.left:
            self.left_menu = LeftMenu(self)
            if self.left_menu.show:
                self.left_menu.pack(side=tk.LEFT, fill=tk.Y, expand=False)

        if self.top:
            self.top_menu = TopMenu(self)
            if self.top_menu.show:
                self.top_menu.pack(side=tk.TOP, fill=tk.X, expand=False)

        self.center.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


if __name__ == "__main__":
    show_splash = True
    if show_splash:
        splash_screen = SplashScreen()
    window = MainWindow()
    window.mainloop()