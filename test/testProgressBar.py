import tkinter as tk
import tkinter.ttk as ttk
import time

from ttkbootstrap import Style

app = tk.Tk()
windowWidth = 400
windowHeight = 200

screenWidth = app.winfo_screenwidth()
screenHeight = app.winfo_screenheight()

x = (screenWidth / 2) - (windowWidth / 2)
y = (screenHeight / 2) - (windowHeight / 2)

print(windowWidth, windowHeight)
print(screenWidth, screenHeight)
print(x,y)
app.overrideredirect(True)


app.title("Splash Screen")
app.geometry(f"{windowWidth}x{windowHeight}+{int(x)}+{int(y)}")
print(f"{windowWidth}x{windowHeight}-+{x}+{y}")

style = Style()

label = ttk.Label(app, text="splash screen", font="Helvetica, 30")
label.pack(pady=50)

progress_bar = ttk.Progressbar(app, value=0, length=300, style='Striped.Horizontal.TProgressbar')
progress_bar.pack(side=tk.BOTTOM, pady=(0, 30))
progress_bar.start(10)
app.after(1060, app.destroy)

app.mainloop()