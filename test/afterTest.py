import tkinter
from tkinter import ttk
from ttkbootstrap import Style


class Application(tkinter.Tk):

    def __init__(self):
        super().__init__()
        self.title('Timer')
        self.style = Style()
        self.geometry('')

        self.nb = tkinter.IntVar()
        self.nb.set(0)

        self.text = tkinter.StringVar()
        self.text.set(self.nb.get())

        self.label = ttk.Label(self, textvariable=self.text, font=(None, 110))
        self.label.pack()

        self.text.set(self.nb.get() + 1)

        self.afterbtn = tkinter.StringVar()

        self.after(1,self.test)
        self.button = ttk.Button(self, text='Start', command=self.test)
        self.button.pack(fill=tkinter.X)

        self.button2 = ttk.Button(self, text='Stop', command=self.stopafter)
        self.button2.pack(fill=tkinter.X)

    def stopafter(self):
        self.after_cancel(self.afterbtn.get())

    def test(self):
        self.nb.set(int(self.text.get()) + 1)
        self.text.set(self.nb.get())
        self.afterbtn.set(self.after(1000,self.test))



if __name__ == '__main__':
    Application().mainloop()