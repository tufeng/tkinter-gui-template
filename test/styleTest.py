from tkinter import *
from tkinter import ttk

class MainPage(ttk.Frame):
    def __init__(self, master):
        ttk.Frame.__init__(self, master)
        self.main_label = ttk.Label(self, text="Pointless Text")
        self.main_button = ttk.Button(self, text="Pointless Button")
        self.main_label.pack(padx=5, pady=5)
        self.main_button.pack(padx=5, pady=5)

        # Configure custom theme
        self.s = ttk.Style()
        self.s.configure('TLabel', foreground='red', background='grey')
        self.s.configure('TButton', foreground='white', background='blue')
        self.s.configure('TFrame', background='black')


root = Tk()
app_frame = MainPage(root)
app_frame.pack()
root.mainloop()