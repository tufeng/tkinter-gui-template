import tkinter as tk
import tkinter.ttk as ttk
from ttkbootstrap import Style


class Application(tk.Tk):

    def __init__(self):
        super().__init__()
        self.title('Timer')
        self.style = Style()

        self.geometry('500x500')

        self.drop = Dropdown(self)
        self.drop2 = Dropdown(self)

        tk.label = ttk.Label(self.drop.get_content(), text="Drop 1")
        tk.label.pack()


class Dropdown(ttk.Frame):
    def __init__(self, *args, **kwargs):
        ttk.Frame.__init__(self, style="primary.TFrame", *args, **kwargs)
        self.pack(fill=tk.BOTH)

        self.is_show = False

        self.dropdown_button_bar = ttk.Frame(self, height=50)
        self.dropdown_button_bar.pack(fill=tk.X, expand=False)

        self.dropdown_button = ttk.Button(self.dropdown_button_bar, text="drop", command=self.show)
        self.dropdown_button.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        self.dropdown = ttk.Frame(self, height=100, style="danger.TFrame")

    def show(self):
        if self.is_show:
            self.is_show = False
            self.dropdown.pack_forget()
        else:
            self.is_show = True
            self.dropdown.pack(fill=tk.X, expand=False)

    def get_content(self):
        return self.dropdown

    def get_button(self):
        return self.dropdown_button


if __name__ == '__main__':
    Application().mainloop()